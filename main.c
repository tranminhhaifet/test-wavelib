#include <stdio.h> 
#include <stdlib.h>
#include <math.h>
#include <wavelib/wavelib.h>

void test_wavedec(){

	printf("test test_wavedec \n");

	int i, J, N, len; 
	int X, Y; 
	wave_object obj; 
	wtree_object wt; 
	double *inp, *oup; 

	char *name = "db3";
	obj = wave_init(name);
	N = 147;
	inp = (double*)malloc(sizeof(double)* N);
	for(int i = 1; i < N + 1; i ++){
		inp[i-1] = -0.25*i*i*i + 25 *i*i + 10*i;
	}
	J = 3; 

	wt = wtree_init(obj, N, J);
	setWTREEExtension(wt, "sym");

	wtree(wt, inp);
	wtree_summary(wt);
	X = 3;
	Y = 5; 
	len = getWTREENodelength(wt, X);
	printf("\n %d", len);
	printf("\n");
	oup = (double*)malloc(sizeof(double)* len);

	printf("Node[%d %d]  Coefficents: \n", X, Y);
	getWTREECoeffs(wt, X, Y, oup, len);
	for(int i = 0; i < len; ++i){
		printf("%g\n", oup[i]);
	}

	free(inp);
	free(oup);
	wave_free(obj);
	wtree_free(wt);

	// return 0;
}



int main () 
{
    printf("test wavelib"); 

    test_wavedec();
    
    return 0; 
}
